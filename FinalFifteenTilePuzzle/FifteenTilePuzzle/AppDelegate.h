//
//  AppDelegate.h
//  FifteenTilePuzzle
//
//  Created by Sean Robbins on 2/5/14.
//  Copyright (c) 2014 Sean Robbins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
