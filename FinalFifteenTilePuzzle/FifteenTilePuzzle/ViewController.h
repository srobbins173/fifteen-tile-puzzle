//  Sean Robbins
//  Febuary 12th 2014
//  Computer Science 470 Adv. Software Development
//  15 Tile Puzzle Project

//      ViewController.h - The header of the view controller decalares all the public properties that are available to the model. It initializes all of the 15 buttons that the model uses to store in an array. As well as the other properties that are used for the functions in the view controller.

//  FifteenTilePuzzle
//  Created by Sean Robbins on 2/5/14.
//  Copyright (c) 2014 Sean Robbins. All rights reserved.


#import <UIKit/UIKit.h>
#import "model.h"


@interface ViewController : UIViewController

@property (nonatomic) IBOutlet UIButton *Tile1;
@property (nonatomic) IBOutlet UIButton *Tile2;
@property (nonatomic) IBOutlet UIButton *Tile3;
@property (nonatomic) IBOutlet UIButton *Tile4;
@property (nonatomic) IBOutlet UIButton *Tile5;
@property (nonatomic) IBOutlet UIButton *Tile6;
@property (nonatomic) IBOutlet UIButton *Tile7;
@property (nonatomic) IBOutlet UIButton *Tile8;
@property (nonatomic) IBOutlet UIButton *Tile9;
@property (nonatomic) IBOutlet UIButton *Tile10;
@property (nonatomic) IBOutlet UIButton *Tile11;
@property (nonatomic) IBOutlet UIButton *Tile12;
@property (nonatomic) IBOutlet UIButton *Tile13;
@property (nonatomic) IBOutlet UIButton *Tile14;
@property (nonatomic) IBOutlet UIButton *Tile15;
@property (nonatomic) IBOutlet UIView *Playboard;
@property (nonatomic) BOOL didMove;
@property (nonatomic) UIButton *prevTile;
@property (nonatomic) int shuffleValue;
@property (strong, nonatomic) IBOutlet UISlider *slider;

@end
