//  Sean Robbins
//  Febuary 12th 2014
//  Computer Science 470 Adv. Software Development
//  15 Tile Puzzle Project

//      Model.m - The model is the brain of the program that helps pass the view controller the valid moves that can be made as well as computing if a win has occured. The first 4 methods each handle a swipe, it will check the blank squares coordinates and compare them to every button in the array. If a valid move is possible it will send back the location in the array if else it returns a number that will not have an outcome. The "check for winner" method is called every time a move has occured after the game has been shuffled. It goes through and compares every buttons current coordinates and compares them to the winning coordinates. If every button passes and a shuffle has occured once & it is not currently shuffling it will tell the view controller to display the alert message. 

//  FifteenTilePuzzle
//  Created by Sean Robbins on 2/10/14.
//  Copyright (c) 2014 Sean Robbins. All rights reserved.


#import "model.h"

@implementation model


-(int) validSwipeDown
{
    
    //Save values of free tile
    int xF = self.blankSquare.origin.x;
    int yF = self.blankSquare.origin.y;
    
    //Move the tile above the free square
    for (int i=0; i<=14;i++)
    {
        //Create a temporary button
        UIButton *temp = [[UIButton alloc]init];
        temp = [self.fifteenTilesArray objectAtIndex:i];
        
        //Save values of temp tile
        int xS = temp.frame.origin.x;
        int yS = temp.frame.origin.y;
        
            if (xS==xF && yS+60==yF)
            {
                return i;
            }
    }
    return 15;
}

-(int) validSwipeUP
{
    int xF = self.blankSquare.origin.x;
    int yF = self.blankSquare.origin.y;
    
    for (int i=0; i<=14;i++)
    {
        UIButton *temp = [[UIButton alloc]init];
        temp = [self.fifteenTilesArray objectAtIndex:i];
        int xS = temp.frame.origin.x;
        int yS = temp.frame.origin.y;
        
        if (xS==xF && yS-60==yF)
        {
          return i;
        }
        
        
    }
return 15;
}

-(int) validSwipeLeft
{
    int xF = self.blankSquare.origin.x;
    int yF = self.blankSquare.origin.y;
    
    for (int i=0; i<=14;i++)
    {
        UIButton *temp = [[UIButton alloc]init];
        temp = [self.fifteenTilesArray objectAtIndex:i];
        int xS = temp.frame.origin.x;
        int yS = temp.frame.origin.y;
        
        if (yS==yF && xS-60==xF)
        {
            return i;
        }
    }
return 15;
}

-(int) validSwipeRight
{
    int xF = self.blankSquare.origin.x;
    int yF = self.blankSquare.origin.y;
    
    
    for (int i=0; i<=14;i++)
    {
        
        UIButton *temp = [[UIButton alloc]init];
        temp = [self.fifteenTilesArray objectAtIndex:i];
        int xS = temp.frame.origin.x;
        int yS = temp.frame.origin.y;
        
        if (yF==yS && xS+60==xF)
        {
          return i;
        }
    }
return 15;
}

-(BOOL)checkForWinner
{
    
//Coordinates to check
int xTile;
int yTile;
int xWin;
int yWin;

//Variable that if added to 15 means every tile is in the correctp place
int Winner=0;

    
    for (int i = 0; i<=14;i++)
    {
    
    UIButton *temp = [self.fifteenTilesArray objectAtIndex:i];
        
    xTile = temp.frame.origin.x;
    yTile = temp.frame.origin.y;
        
    xWin = [[self.winningX objectAtIndex:i]integerValue];
    yWin = [[self.winningY objectAtIndex:i]integerValue];
    
        
        if (xTile == xWin && yTile ==yWin)
        {
            Winner++;
        }
    }
    
    //Only display message if
        //All tiles are in the winning position
        //You are not currently shuffling the tiles
        //The game has shuffled at least once
    if (Winner==15 && self.hasShuffled == YES && self.shufflingTiles == NO)
    {
        //NSLog(@"You have won");
        self.hasShuffled = NO;
        return YES;
    }

return NO;
}


@end
