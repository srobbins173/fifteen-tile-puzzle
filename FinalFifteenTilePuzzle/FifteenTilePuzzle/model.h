//  Sean Robbins
//  Febuary 12th 2014
//  Computer Science 470 Adv. Software Development
//  15 Tile Puzzle Project

//      Model.h- The header of the model declares the properties that will be used in its functions. The first 2 properties are used in declaring if a win has occured. The NSArray fifteenTilesArray is what is used in most functions in order to run through each button. The blankSquare property is used to keep track of the blank space. The final 2 Arrays are used to store where the winning x & y coordinates are for the winning and reset functions. 

//  FifteenTilePuzzle
//  Created by Sean Robbins on 2/10/14.
//  Copyright (c) 2014 Sean Robbins. All rights reserved.


#import <Foundation/Foundation.h>
#import "ViewController.h"

@interface model : NSObject

@property (nonatomic) BOOL shufflingTiles; 
@property (nonatomic) BOOL hasShuffled;
@property (nonatomic) NSArray *fifteenTilesArray;
@property (nonatomic) CGRect blankSquare;         
@property (nonatomic) NSArray *winningX;
@property (nonatomic) NSArray *winningY;

-(int)validSwipeDown;
-(int)validSwipeUP;
-(int)validSwipeLeft;
-(int)validSwipeRight;
-(BOOL)checkForWinner;



@end
