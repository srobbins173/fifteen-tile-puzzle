//
//  tileBrain.m
//  FifteenTilePuzzle
//
//  Created by Sean Robbins on 2/7/14.
//  Copyright (c) 2014 Sean Robbins. All rights reserved.
//

#import "tileBrain.h"


@implementation tileBrain : NSObject



-(int)returnX:(int)x {
    
    if (x==0)
        return 0;
    
    if (x==1)
        return 60;
    
    if (x==2)
        return 120;
    
    if (x==3)
        return 180;
    
    if (x==4)
        return 0;
    
    if (x==5)
        return 60;
    
    if (x==6)
        return 120;
    
    if (x==7)
        return 180;
    
    if (x==8)
        return 0;
    
    if (x==9)
        return 60;
    
    if (x==10)
        return 120;
    
    if (x==11)
        return 180;
    
    if (x==12)
        return 0;
    
    if (x==13)
        return 60;
    
    if (x==14)
        return 120;
    
    
    return 0;
    
}

-(int)returnY:(int)x {
    
    if (x==0)
        return 0;
    
    if (x==1)
        return 0;
    
    if (x==2)
        return 0;
    
    if (x==3)
        return 0;
    
    if (x==4)
        return 60;
    
    if (x==5)
        return 60;
    
    if (x==6)
        return 60;
    
    if (x==7)
        return 60;
    
    if (x==8)
        return 120;
    
    if (x==9)
        return 120;
    
    if (x==10)
        return 120;
    
    if (x==11)
        return 120;
    
    if (x==12)
        return 180;
    
    if (x==13)
        return 180;
    
    if (x==14)
        return 180;
    
    
    return 0;
    
}





@end
