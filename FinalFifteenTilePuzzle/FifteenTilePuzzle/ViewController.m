//  Sean Robbins
//  Febuary 12th 2014
//  Computer Science 470 Adv. Software Development
//  15 Tile Puzzle Project

//  ViewController.m - The View Controller is in charge of all the "actions" of the application. Meaning that that is will deligate the buttons on the storyboard. Initially it will initialize all the swipe functions that the user  can do to move the tile pieces. Next the model properties are created such as the corrent location of the blank square, the array of buttons and 2 arrays of winning locations for the buttons.

//      The "swipe recognized" function will recognize what direction has occured and pass it to the corresponding method in the model to dictate what button if possible should be swapped with the free space. If so it will call the "swapped tile" method if this is valid.

//      The "swapped tile" function is the core feature of this program that most methods act upon. It will take the coordinates of the free square and the coordinates of the tiles that the swipe method has given it and exchange the two.It will then mark that at least one move has been made (so a winning message doesnt just appear on start up). And save the button that was the most recently moved (for shuffle). After every move it will check if it has won as long as at least one shuffle has occured.

//      The "adjust slider" function simple takes the value passed in by the slider and saves it as a property to be used in the shuffle function.

//      The "touched shuffle" button is activated when the user tappes the shuffle button. It works by taking in the slider value and making sure that a move occurs that many times. It produces a random tile from the tile array and tests if that if that move is possible (as long as it wasnt the last tile moved to avoid redundancy). Once it is done shuffling it changes the didMove property (for check winner), it marks that a shuffle has occured at least once, and says that shuffling has stopped (to not just pop up winning message while shuffling).

//      The "touched reset" function is activated when the the reset button has been tapped. It first resets were the blank square should be. Then it grabs button 1, the x & y coordinate to be moved to (its the same as where it should be to win). Then places it in the correct spot.

//      The "show message" function simply displays a UIAlerMessage when the hasWon method in the model calls it.

//      The "has Won" function simply calles the method in the model to check for a winner.

//  FifteenTilePuzzle
//  Created by Sean Robbins on 2/5/14.
//  Copyright (c) 2014 Sean Robbins. All rights reserved.


#import "ViewController.h"

@interface ViewController ()

@property (nonatomic) model *model;

@end

@implementation ViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.didMove = NO;
    self.model.hasShuffled  = NO;
    self.model.shufflingTiles = NO;
 
    //Initialize the left swipe function
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRecognized:)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:swipeLeft];
    
    //Initialize the right swipe function
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRecognized:)];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:swipeRight];
    
    //Initialize the down swipe function
    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRecognized:)];
    [swipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.view addGestureRecognizer:swipeDown];
    
    //Initialize the up swipe function
    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRecognized:)];
    [swipeUp setDirection:UISwipeGestureRecognizerDirectionUp];
    [self.view addGestureRecognizer:swipeUp];
    
    
    //Create the model object
    self.model = [[model alloc]init];
    
    //Initialize the location of blank square
    self.model.blankSquare = CGRectMake(180, 180, 60, 60);
    
    //Array of each button
    self.model.fifteenTilesArray = [[NSArray alloc]initWithObjects:_Tile1,_Tile2,_Tile3,_Tile4,_Tile5,_Tile6,_Tile7,_Tile8, _Tile9,_Tile10,_Tile11,_Tile12,_Tile13,_Tile14,_Tile15, nil];
    
    //Initialize the Winning Coordinate Arrays
    
    self.model.winningY = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:60],[NSNumber numberWithInt:60],[NSNumber numberWithInt:60],[NSNumber numberWithInt:60],[NSNumber numberWithInt:120],[NSNumber numberWithInt:120],[NSNumber numberWithInt:120],[NSNumber numberWithInt:120],[NSNumber numberWithInt:180],[NSNumber numberWithInt:180],[NSNumber numberWithInt:180], nil];
    
    
    self.model.winningX = [[NSArray alloc]initWithObjects:[NSNumber numberWithInt:0],[NSNumber numberWithInt:60],[NSNumber numberWithInt:120],[NSNumber numberWithInt:180],[NSNumber numberWithInt:0],[NSNumber numberWithInt:60],[NSNumber numberWithInt:120],[NSNumber numberWithInt:180],[NSNumber numberWithInt:0],[NSNumber numberWithInt:60],[NSNumber numberWithInt:120],[NSNumber numberWithInt:180],[NSNumber numberWithInt:0],[NSNumber numberWithInt:60],[NSNumber numberWithInt:120], nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) swipeRecognized:(UISwipeGestureRecognizer*)swipe
{
    
        if (swipe.direction == UISwipeGestureRecognizerDirectionDown)
        {
            int buttonIndex = [self.model validSwipeDown];
            
            if (buttonIndex <=14){
               [self swappedTile:[self.model.fifteenTilesArray objectAtIndex:buttonIndex]];
            }
            
        }
        
        if (swipe.direction == UISwipeGestureRecognizerDirectionUp)
        {
            int buttonIndex = [self.model validSwipeUP];
            
            if (buttonIndex <=14){
                [self swappedTile:[self.model.fifteenTilesArray objectAtIndex:buttonIndex]];
            }
        }
            
        if (swipe.direction == UISwipeGestureRecognizerDirectionLeft)
        {
            int buttonIndex = [self.model validSwipeLeft];
            
            if (buttonIndex <=14){
                [self swappedTile:[self.model.fifteenTilesArray objectAtIndex:buttonIndex]];
            }
        }
    
        if (swipe.direction == UISwipeGestureRecognizerDirectionRight)
        {
            int buttonIndex = [self.model validSwipeRight];
            
            if (buttonIndex <=14){
                [self swappedTile:[self.model.fifteenTilesArray objectAtIndex:buttonIndex]];
            }
        }
}

- (void)swappedTile:(UIButton *)sender
{
  
//Save values of free tile
int xF = self.model.blankSquare.origin.x;
int yF = self.model.blankSquare.origin.y;
    
//Save values of sender tile
int xS = sender.frame.origin.x;
int yS = sender.frame.origin.y;

     if (  (xS==xF && yS+60==yF) ||
           (yF==yS && xS+60==xF) ||
           (xF==xS && yS-60==yF) ||
           (yS==yF && xS-60==xF)  )
    {
        [UIView animateWithDuration:.5 animations:^{
            //Move tile
            sender.frame = self.model.blankSquare;
            //Change blankSquare
            self.model.blankSquare =CGRectMake(xS,yS, 60, 60);
        }];
        
        //Make a Move occured
        self.didMove=YES;
        
        //Save as most recent tile moved
        self.prevTile = sender;
        
       
        if (self.model.hasShuffled ==YES)
        {
            //Check if Player has won
            [self hasWon];
        }
        
        
    }
}


- (IBAction)adjSluder:(id)sender
{
    //Change the shuffle value property
    self.shuffleValue = [_slider value];

}

- (IBAction)touchedShuffle:(id)sender {
    
    
    int randomNUmber=0;
    int min=0;
    int max=14;
    self.model.shufflingTiles = YES;
    
    //Make 20 moves
    for (int timesToShuffle = 0; timesToShuffle<self.shuffleValue;timesToShuffle++)
    {
        //While a move has not been made
        while(self.didMove==NO)
        {
            //Generate a random number to shuffle
            randomNUmber = min + arc4random() % (max - min + 1);
            
            while ([self.model.fifteenTilesArray objectAtIndex:randomNUmber] == self.prevTile)
            {
                //Keep generating numbers until it is different from the previous move
                randomNUmber = min + arc4random() % (max - min + 1);
            }
            
            //Move that valid tile for shuffling
            [self swappedTile:[self.model.fifteenTilesArray objectAtIndex:randomNUmber]];
            
        }
        
        //Pause for the next move
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:.5]];
        
        
        //Reset the didMove variable
        self.didMove=NO;
        
    }
    
    //No longer shuffling tiles
    self.model.shufflingTiles = NO;
    
    //You have shuffled at least once 
    self.model.hasShuffled = YES;
   
}

- (IBAction)touchedReset:(id)sender
{
int newX;
int newY;
self.model.blankSquare = CGRectMake(180, 180, 60, 60);
CGRect newSpot;
    
    for (int i = 0;i<15;i++)
    {
        
        UIButton *currentTile = [self.model.fifteenTilesArray objectAtIndex:i];
        newX=[[self.model.winningX objectAtIndex:i]integerValue];
        newY=[[self.model.winningY objectAtIndex:i]integerValue];
        
        newSpot = CGRectMake(newX, newY, 60, 60);
        [UIView animateWithDuration:.5 animations:^{
        currentTile.frame = newSpot;
        }];
       
    }
}

-(void)showMessage:(id)sender
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"You Won !!!"
                                                      message:@"Increase difficulty to really test your skillz"
                                                     delegate:nil
                                            cancelButtonTitle:@"BAZINGA :)"
                                            otherButtonTitles:nil];
    
    [message show];
}

-(void) hasWon
{
    BOOL winner = [self.model checkForWinner];
    if (winner==YES)
    {
    [self showMessage:nil];
    }

}



@end
